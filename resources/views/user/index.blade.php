@extends('layout.admin')

@section('tittle')
  User
@endsection

@section('content')

<a href="/user/create" class="btn btn-primary mb-2">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Email</th>
                <th scope="col">Jam masuk</th>
                <th scope="col">Jam keluar</th>
                <th scope="col">Whatsapp</th>
                <th scope="col">Address</th>
                <th scope="col">Detailed</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($user as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->name}}</td>
                        <td>{{$value->email}}</td>
                        <td>{{$value->jam_masuk}}</td>
                        <td>{{$value->jam_keluar}}</td>
                        <td>{{$value->whatsapp}}</td>
                        <td>{{$value->address}}</td>
                        <td>
                            <a href="/user/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/user/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/user/{{$value->id}}" method="POST">
                                 @csrf
                                 @method('DELETE')
                                 <input type="submit" class="btn btn-danger my-1" value="Delete">
                             </form>
                         </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>
                @endforelse
            </tbody>
        </table>

@endsection
