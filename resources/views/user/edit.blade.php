@extends('layout.admin')

@section('title')
    Edit User
@endsection

@section('content')
<div>
    <form action="/user/{{$user->id}}" method="POST">
        @method('put')
        @csrf
        <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="name" value="{{$user->name}}" placeholder="Masukkan Body">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" name="email" value="{{$user->email}}" placeholder="Masukkan Body">
            @error('email')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Jam masuk</label>
            <input type="text" class="form-control" name="jam_masuk" value="{{$user->jam_masuk}}" placeholder="Masukkan Body">
            @error('jam_masuk')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Jam keluar</label>
            <input type="text" class="form-control" name="jam_keluar" value="{{$user->jam_keluar}}" placeholder="Masukkan Body">
            @error('jam_keluar')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Whatsapp</label>
            <input type="text" class="form-control" name="whatsapp" value="{{$user->whatsapp}}" placeholder="Masukkan Body">
            @error('whatsapp')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Address</label>
            <input type="text" class="form-control" name="address" value="{{$user->address}}" placeholder="Masukkan Body">
            @error('address')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection
