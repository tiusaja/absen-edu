@extends('layout.admin')

@section('title')
    Learns
@endsection

@section('content')

<a href="/learn/create" class="btn btn-primary">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Date Start</th>
                <th scope="col">Date End</th>
                <th scope="col">Type</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($learn as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->datestart}}</td>
                        <td>{{$value->dateend}}</td>
                        <td>{{$value->students_id}}</td>
                        <td>
                            <form action="/posts/{{$value->id}}" method="POST">
                                <a href="/posts/{{$value->id}}" class="btn btn-info">Show</a>
                                <a href="/posts/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection