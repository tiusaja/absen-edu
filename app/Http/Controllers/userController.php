<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class userController extends Controller
{
    public function index(){
        $user= DB::table('user')->get();
        return view('user.index', compact('user'));
    }

    public function create(){
        return view('user.create');
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'jam_masuk' => 'required',
            'jam_keluar' => 'required',
            'whatsapp' => 'required',
            'address' => 'required',
        ]);
        $query = DB::table('user')->insert([
            "name" => $request["name"],
            "email" => $request["email"],
            "jam_masuk" => $request["jam_masuk"],
            "jam_keluar" => $request["jam_keluar"],
            "whatsapp" => $request["whatsapp"],
            "address" => $request["address"]
        ]);
        return redirect('/user');
    }

    public function show($id){
        $user= DB::table('user')->where('id', $id)->first();
        return view('user.show', compact('user'));
    }

    public function edit($id){
        $user= DB::table('user')->where('id', $id)->first();
        return view('user.edit', compact('user'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'jam_masuk' => 'required',
            'jam_keluar' => 'required',
            'whatsapp' => 'required',
            'address' => 'required',
        ]);
        $query = DB::table('user')
            ->where('id', $id)
            ->update([
            "name" => $request["name"],
            "email" => $request["email"],
            "jam_masuk" => $request["jam_masuk"],
            "jam_keluar" => $request["jam_keluar"],
            "whatsapp" => $request["whatsapp"],
            "address" => $request["address"]
            ]);
        return redirect('/user');
    }

    public function destroy($id){
        $query = DB::table('user')->where('id', $id)->delete();
        return redirect('/user');
    }
}
