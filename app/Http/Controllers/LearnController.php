<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Learn;
use App\Student;

class LearnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $table = Learn::all();
        return view('learns.index', compact('learns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students=Student::all();
       return view('learns.create', compact('students'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'datestart' => 'required|unique:students',
            'dateend' => 'required',
            'students_id' => 'required'
        ]);

        $learn = Learn::create([
            'datestart'=>$request->datestart,
            'dateend'=>$request->dateend,
            'students_id'=>$request->students_id,
        ]);

        return redirect('/learn');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Learn  $learn
     * @return \Illuminate\Http\Response
     */
    public function show(Learn $learn)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Learn  $learn
     * @return \Illuminate\Http\Response
     */
    public function edit(Learn $learn)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Learn  $learn
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Learn $learn)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Learn  $learn
     * @return \Illuminate\Http\Response
     */
    public function destroy(Learn $learn)
    {
        //
    }
}
