<?php
  
use Illuminate\Database\Seeder;
use App\User;
   
class CreateUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name'=>'Admin01',
                'email'=>'adi.priyanto.1110@gmail.com',
                 'is_admin'=>'1',
                 'is_student'=>'0',
                 'password'=> bcrypt('123456'),
            ],
            [
                'name'=>'Admin02',
                'email'=>'nugrahalazuardim@gmail.com',
                'is_admin'=>'1',
                'is_student'=>'0',
                'password'=> bcrypt('123456'),
            ],
            [
                'name'=>'Admin03',
                'email'=>'rmbryanas@gmail.com',
                 'is_admin'=>'1',
                 'is_student'=>'0',
                 'password'=> bcrypt('123456'),
            ],
            [
                'name'=>'Student01',
                'email'=>'student01@gmail.com',
                'is_admin'=>'0',
                'is_student'=>'0',
                 'password'=> bcrypt('123456'),
            ],
            [
               'name'=>'User',
               'email'=>'user@itsolutionstuff.com',
                'is_admin'=>'0',
                'is_student'=>'0',
               'password'=> bcrypt('123456'),
            ],
        ];
  
        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}